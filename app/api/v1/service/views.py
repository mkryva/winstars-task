from rest_framework.generics import ListAPIView
from rest_framework.permissions import AllowAny

from app.api.v1.service.serializers import ServiceSerializer
from app.service.models import Service


class ServiceListView(ListAPIView):
    """Class that describe list view for services"""
    serializer_class = ServiceSerializer
    permission_classes = [AllowAny]
    queryset = Service.objects.all()