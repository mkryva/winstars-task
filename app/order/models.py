from django.db import models

from app.service.models import Service


class Order(models.Model):
    service = models.ForeignKey(Service,
                                on_delete=models.CASCADE,
                                related_name='jobs')
    STATUS_CHOICES = (
        (0, 'Waiting'),
        (1, 'In progress'),
        (2, 'Done'),
    )
    status = models.IntegerField(
        choices=STATUS_CHOICES,
        default=0,
    )

