# -*- coding: utf-8 -*-
# pylint: disable=missing-docstring
from __future__ import unicode_literals

from django.apps import AppConfig

class UserConfig(AppConfig):
    name = 'app.user'
