from rest_framework import serializers
from app.service.models import Service


class ServiceSerializer(serializers.HyperlinkedModelSerializer):
    """
    Class that describes job serializer
    """

    class Meta:
        model = Service
        fields = ('id', 'name', 'description', 'price')