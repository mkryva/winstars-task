from django.conf.urls import url

from app.api.v1.service.views import ServiceListView


urlpatterns = [
    url(r'^', ServiceListView.as_view(), name='services'),
]