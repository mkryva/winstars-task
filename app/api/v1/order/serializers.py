from rest_framework import serializers

from app.order.models import Order


class OrderSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Order
        fields = ('id', 'service', 'status')