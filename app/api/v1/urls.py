from django.conf.urls import url, include

urlpatterns = [
    url(r'^auth/', include('app.api.v1.staff.urls')),
]