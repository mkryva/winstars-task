from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from app.api.v1.staff.perms import AdminAuthenticationPermission
from app.api.v1.staff.serializers import UserSerializer


@api_view(['POST'])
@permission_classes((AdminAuthenticationPermission,))
def create_user(request):
    serialized = UserSerializer(data=request.data)
    if serialized.is_valid():
        serialized.save()
        return Response(serialized.data, status=status.HTTP_201_CREATED)
    else:
        return Response(serialized._errors, status=status.HTTP_400_BAD_REQUEST)