from rest_framework import permissions


class AdminAuthenticationPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        user = request.user
        if user and user.is_authenticated():
            return user.is_superuser
        return False