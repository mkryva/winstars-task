from django.conf.urls import url, include
from django.contrib.auth.models import AbstractUser

from app.api.v1.staff.views import create_user

urlpatterns = [
    url(r'^', include('rest_auth.urls')),
    url(r'^hello/$', create_user),
]
