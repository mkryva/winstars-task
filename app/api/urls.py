from django.conf.urls import url
from django.urls import include

urlpatterns = [
    url(r'^', include('app.api.v1.urls')),
    url(r'^v1/', include('app.api.v1.urls')),

]