from django.db import models


class Service(models.Model):
    name = models.CharField(max_length=40, blank=False)
    description = models.TextField(max_length=255)
    price = models.IntegerField()
